package code

import java.io.File

class Deck(private val cards: List<Int>) {

    fun hasCards() = cards.isNotEmpty()

    fun playCard() = cards[0] to Deck(cards.drop(1))

    fun addCards(winnersCard: Int, losersCard: Int) = Deck(cards + winnersCard + losersCard)

    fun scoreDeck() = cards.reversed().foldIndexed(0L, {idx, totalScore, value -> totalScore + ((1 + idx) * value)})
}

fun constructDeck(inputCards: String) = Deck(inputCards.split("\n").map { it.toInt() })

fun playGame(p1Initial: Deck, p2Initial: Deck) : Pair<Deck, Deck> {
    var player1Deck = p1Initial
    var player2Deck = p2Initial
    while (player1Deck.hasCards() && player2Deck.hasCards()) {
        val (p1Card, p1tempDeck) = player1Deck.playCard()
        val (p2Card, p2tempDeck) = player2Deck.playCard()
        if (p1Card > p2Card) {
            player1Deck = p1tempDeck.addCards(p1Card, p2Card)
            player2Deck = p2tempDeck
        } else {
            player2Deck = p2tempDeck.addCards(p2Card, p1Card)
            player1Deck = p1tempDeck
        }
    }
    return player1Deck to player2Deck
}


fun main() {
    val (p1Lines, p2Lines) = File("/Users/pbrady/dev/kotlinAdvent2020/src/inputs/day22.txt").readText().split("\n\n")
    val (p1Deck, p2Deck) = (playGame(constructDeck(p1Lines.substringAfter(":\n")), constructDeck(p2Lines.substringAfter(":\n"))))
    println(p1Deck.scoreDeck() + p2Deck.scoreDeck())
}