package code

import java.io.File

fun main() {
    val lines = File("/Users/pbrady/dev/kotlinAdvent2020/src/inputs/day10.txt")
            .useLines { it.toList() }.map { s -> s.toInt() }.sorted()
    var prevJolt = 0
    val joltTracker = mutableMapOf<Int, Int>()
    for (jolt in lines) {
        joltTracker[jolt - prevJolt] = 1 + joltTracker.getOrElse(jolt - prevJolt, { 0 })
        prevJolt = jolt
    }
    joltTracker[3] = 1 + joltTracker.getOrElse(3, {0}) // Final adapter
    println(joltTracker)
    println(joltTracker[1]?.times(joltTracker[3]!!))


    println(arrangements(lines.plus(0).sorted()))


}

fun arrangements(lines: List<Int>): Long {
    val indexToOptions = mutableMapOf<Int, Long>(Pair(lines.last() + 3, 1))
    for (i in lines.last() downTo 0) {
        if (!lines.contains(i)) continue
        indexToOptions[i] = indexToOptions.getOrDefault(i+1, 0) +
                indexToOptions.getOrDefault(i+2, 0) +
                indexToOptions.getOrDefault(i+3, 0)
    }
    println(indexToOptions)
    return indexToOptions[0]!!
}