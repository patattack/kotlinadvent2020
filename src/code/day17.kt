package code

import java.io.File

data class Quadruple(val x: Int, val y: Int, val z: Int, val w: Int)

class HyperGrid(private val g: Map<Quadruple, Boolean>) {
    constructor() : this(emptyMap<Quadruple, Boolean>())

    fun setActive(locations: List<Quadruple>) : HyperGrid {
        return HyperGrid(g.plus(locations.map { it to true }))
    }

    fun computeCycle() : HyperGrid {
        val lowerExtremes = Quadruple(
                g.keys.reduce {t1, t2 -> if (t1.x < t2.x) t1 else t2}.x - 1,
                g.keys.reduce {t1, t2 -> if (t1.y < t2.y) t1 else t2}.y - 1,
                g.keys.reduce {t1, t2 -> if (t1.z < t2.z) t1 else t2}.z - 1,
                g.keys.reduce {t1, t2 -> if (t1.w < t2.w) t1 else t2}.w - 1)
        val upperExtremes = Quadruple(
                g.keys.reduce {t1, t2 -> if (t1.x > t2.x) t1 else t2}.x + 1,
                g.keys.reduce {t1, t2 -> if (t1.y > t2.y) t1 else t2}.y + 1,
                g.keys.reduce {t1, t2 -> if (t1.z > t2.z) t1 else t2}.z + 1,
                g.keys.reduce {t1, t2 -> if (t1.w > t2.w) t1 else t2}.w + 1)

        val checkableLocations = createHyperLocations(lowerExtremes, upperExtremes)
        val activeLocations = checkableLocations.fold(emptyList<Quadruple>(), {
            activeLocs, candidate ->
            if (g.getOrDefault(candidate, false)) {
                // Active
                if (countActiveNeighbors(candidate) in 2..3) activeLocs + candidate
                else activeLocs
            } else {
                // Inactive
                if (countActiveNeighbors(candidate) == 3) activeLocs + candidate
                else activeLocs
            }
        })

        return HyperGrid(mapOf(*activeLocations.map { it to true }.toTypedArray()))
    }

    private fun countActiveNeighbors(point: Quadruple) : Int {
        val nearbyCoords = createHyperLocations(Quadruple(point.x - 1, point.y - 1, point.z - 1, point.w - 1),
                Quadruple(point.x + 1, point.y + 1, point.z + 1, point.w + 1))
        return (nearbyCoords - point).filter { g.getOrDefault(it, false) }.count()
    }

    fun countActiveCubes() = g.count()
}

class Grid(private val g: Map<Triple<Int, Int, Int>, Boolean>) {
    constructor() : this(emptyMap<Triple<Int, Int, Int>, Boolean>())

    fun setActive(vararg locations: Triple<Int, Int, Int>) : Grid {
        return Grid(g.plus(locations.map { it to true }))
    }

    fun computeCycle() : Grid {
        val lowerExtremes = Triple(
                g.keys.reduce {t1, t2 -> if (t1.first < t2.first) t1 else t2}.first - 1,
                g.keys.reduce {t1, t2 -> if (t1.second < t2.second) t1 else t2}.second - 1,
                g.keys.reduce {t1, t2 -> if (t1.third < t2.third) t1 else t2}.third - 1)
        val upperExtremes = Triple(
                g.keys.reduce {t1, t2 -> if (t1.first > t2.first) t1 else t2}.first + 1,
                g.keys.reduce {t1, t2 -> if (t1.second > t2.second) t1 else t2}.second + 1,
                g.keys.reduce {t1, t2 -> if (t1.third > t2.third) t1 else t2}.third + 1)

        val checkableLocations = createLocationsWithinRanges(lowerExtremes, upperExtremes)
        val activeLocations = checkableLocations.fold(emptyList<Triple<Int, Int, Int>>(), {
            activeLocs, candidate ->
            if (g.getOrDefault(candidate, false)) {
                // Active
                if (countActiveNeighbors(candidate) in 2..3) activeLocs + candidate
                else activeLocs
            } else {
                // Inactive
                if (countActiveNeighbors(candidate) == 3) activeLocs + candidate
                else activeLocs
            }
        })

        return Grid(mapOf(*activeLocations.map { it to true }.toTypedArray()))
    }

    private fun countActiveNeighbors(point: Triple<Int, Int, Int>) : Int {
        val nearbyCoords = createLocationsWithinRanges(Triple(point.first - 1, point.second - 1, point.third - 1),
                Triple(point.first + 1, point.second + 1, point.third + 1))
        return (nearbyCoords - point).filter { g.getOrDefault(it, false) }.count()
    }

    fun countActiveCubes() = g.count()
}

fun createHyperLocations(lowest: Quadruple, highest: Quadruple) : List<Quadruple> {
    return (lowest.w .. highest.w).flatMap { wDimension ->
        val locations = createLocationsWithinRanges(Triple(lowest.x, lowest.y, lowest.z), Triple(highest.x, highest.y, highest.z))
        locations.map {  Quadruple(it.first, it.second, it.third, wDimension) }
    }
}

fun createLocationsWithinRanges(lowest: Triple<Int, Int, Int>, highest: Triple<Int, Int, Int>) : List<Triple<Int, Int, Int>> {
    return (lowest.first .. highest.first).flatMap { i ->
        (lowest.second .. highest.second).flatMap { j ->
            (lowest.third .. highest.third).map { k ->
                Triple(i, j, k)
            }
        }
    }
}

fun constructGrid(lines: List<String>) : Grid {
    val indexed = lines.mapIndexed {rowIdx, row -> row.mapIndexed{colIdx, c -> Triple(rowIdx, colIdx, 0) to c}}
    val activeLocations = indexed.flatMap { row -> row.filter { it.second == '#' }.map { it.first } }
    return Grid().setActive(*activeLocations.toTypedArray())
}

fun constructHyperGrid(lines: List<String>) : HyperGrid {
    val indexed = lines.mapIndexed {rowIdx, row -> row.mapIndexed{colIdx, c -> Quadruple(rowIdx, colIdx, 0, 0) to c}}
    val activeLocations = indexed.flatMap { row -> row.filter { it.second == '#' }.map { it.first } }
    return HyperGrid().setActive(activeLocations)
}

fun main() {
    val lines = File("/Users/pbrady/dev/kotlinAdvent2020/src/inputs/day17.txt").readLines()
    var oldGrid = constructGrid(lines)
    repeat(6 ) {
        val newGrid = oldGrid.computeCycle()
        oldGrid = newGrid
        println(oldGrid.countActiveCubes())
    }
    println(oldGrid.countActiveCubes())


    // Part 2
    var oldHyperGrid = constructHyperGrid(lines)
    repeat(6 ) {
        val newGrid = oldHyperGrid.computeCycle()
        oldHyperGrid = newGrid
        println(oldHyperGrid.countActiveCubes())
    }
    println(oldHyperGrid.countActiveCubes())

}