package code

import java.io.File

class CupGame(val nums: List<Int>) {

    fun playRound(currentCupIndex: Int) : Pair<Int, CupGame> {
        val currentCupVal = nums[currentCupIndex]
        val pickedCups = listOf(
                nums[(currentCupIndex + 1).rem(nums.size)],
                nums[(currentCupIndex + 2).rem(nums.size)],
                nums[(currentCupIndex + 3).rem(nums.size)])
        val remainingCups = nums - pickedCups
        val destVal = findDestinationValue(currentCupVal, remainingCups)
        val destIndex = remainingCups.indexOf(destVal)
//        val newOrder = (nums - pickedCups).fold(emptyList<Int>(), {
//            l, v -> if (v == destVal) l + v + pickedCups else l + v
//        })
        val newOrder = remainingCups.subList(0, destIndex + 1) + pickedCups + remainingCups.subList((destIndex + 1), remainingCups.size)
        return (newOrder.indexOf(currentCupVal) + 1).rem(nums.size) to CupGame(newOrder)

    }

    private fun findDestinationValue(currentCupVal: Int, remainingCups: List<Int>) : Int {
        return when (remainingCups.find { it < currentCupVal }) {
            null -> remainingCups.max() ?: -1
            else -> {
                val sorted = remainingCups.sorted()
                val sortedIdx = sorted.indexOf(currentCupVal)
                sorted[sortedIdx - 1]
            }
        }
    }

    fun playNumTimes(times: Int, initialCupIndex: Int) : CupGame {
        var currentGame = this
        var currentCupIndex = initialCupIndex
        repeat(times) {
            val (nextIndex, newGame) = currentGame.playRound(currentCupIndex)
            currentGame = newGame
            currentCupIndex = nextIndex
            if (it.rem(10000) == 0) println(it)
        }
        return currentGame
    }
}

fun main() {
    val nums =  File("/Users/pbrady/dev/kotlinAdvent2020/src/inputs/day23.txt").readText().map { Character.getNumericValue(it)}
    println(nums)
//    val finalState = CupGame(nums).playNumTimes(10, 0)
//    println(finalState.nums)

    val biggerDeck = nums + ((nums.max()!! + 1) .. 1000000).toList()
    val biggerFinalState = CupGame(biggerDeck).playNumTimes(1000000, 0)
    val oneIndex = biggerFinalState.nums.indexOf(1)
    println(biggerFinalState.nums[oneIndex + 1].toLong())
    println(biggerFinalState.nums[oneIndex + 1].toLong())
    println(biggerFinalState.nums[oneIndex + 1].toLong() * biggerFinalState.nums[oneIndex + 2].toLong())
}