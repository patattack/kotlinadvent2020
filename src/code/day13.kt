package code

import java.io.File

fun main() {
    val lines = File("/Users/pbrady/dev/kotlinAdvent2020/src/inputs/day13.txt")
            .useLines { it.toList() }
    val targetTime = lines[0].toInt()

    // Part 1
    var busInfo = lines[1].split(',')
            .filter { s -> s != "x" }
            .map { s -> Pair(s.toInt(), s.toInt() - targetTime.rem(s.toInt())) }
            .minBy { pair -> pair.second }!!
    println(busInfo)
    println(busInfo.first * busInfo.second)

    // Part 2

}