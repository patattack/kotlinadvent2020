package code

import java.io.File

fun main() {
    val lines = File("/Users/pbrady/dev/kotlinAdvent2020/src/inputs/day6.txt").readText()

    println(anyoneYes(lines).sum())
    println(everyoneYes(lines).sum())
}

fun everyoneYes(input: String): List<Int> {
    return input.split("\n\n").map {
        group ->
        val numPeople = 1 + group.trim().count { c -> c == '\n' }
        group.filter { c -> c in 'a'..'z' }.toCharArray().fold(
                emptyMap<Char, Int>(), {
                    map, c ->
                    map.plus(Pair(c, 1 + map.getOrElse(c, { 0 })))
                }).filter { entry -> entry.value == numPeople }.count()
    }

}

fun anyoneYes(input: String): List<Int> {
    return input.split("\n\n").map {
        group ->
        group.filter { c -> c in 'a'..'z' }.toCharArray().fold(emptySet<Char>(), {set, c -> set.plus(c)}).count()
    }
}