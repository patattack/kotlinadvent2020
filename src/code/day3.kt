package code

import java.io.File

fun main() {
    val lines = File("/Users/pbrady/dev/kotlinAdvent2020/src/inputs/day3.txt")
        .useLines { it.toList() }

    val slopes = listOf(
        computeNumTrees(lines, 1, 1),
        computeNumTrees(lines, 3, 1),
        computeNumTrees(lines, 5, 1),
        computeNumTrees(lines, 7, 1),
        computeNumTrees(lines, 1, 2)
    )
    println(slopes)
    println(slopes.foldRight( 1L, {total, next -> total * next}))
}

private fun computeNumTrees(lines: List<String>, rightShift: Int, downShift: Int): Int {
    val lineLength = lines.first().length
    var trees = 0
    var horizontalIndex = 0
    var lineIndex = 0
    while (lineIndex < lines.size) {
        val line = lines[lineIndex]
        if (line[horizontalIndex % lineLength] == '#') trees++
        horizontalIndex += rightShift
        lineIndex += downShift
    }
    return trees
}