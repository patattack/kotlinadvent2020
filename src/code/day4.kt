package code

import java.io.File
import java.util.regex.Pattern

fun main() {
    testThings()
    println(File("/Users/pbrady/dev/kotlinAdvent2020/src/inputs/day4.txt").readText()
            .split("\n\n")
            .map { passportString ->
                passportString.replace('\n', ' ').split(' ')
                        .map { keyValString -> Pair(keyValString.split(':')[0], keyValString.split(':')[1]) }
            }.filter { passport: List<Pair<String, String>> -> isValidPassport(passport) }.count())
}

fun isValidPassport(passport: List<Pair<String, String>>): Boolean {
    val passportKeys = passport.map { pair -> pair.first }
    return setOf(
            Pair("byr", {year: String -> isValidBYR(year)}),
            Pair("iyr", {year: String -> isValidIYR(year)}),
            Pair("eyr", {year: String -> isValidEYR(year)}),
            Pair("hgt", {height: String -> isValidHGT(height)}),
            Pair("hcl", {color: String -> isValidHCL(color)}),
            Pair("ecl", {color: String -> isValidECL(color)}),
            Pair("pid", {num: String -> isValidPID(num)})
    ).map { requirement -> requirement.second(passport.find { pair -> pair.first == requirement.first }?.second.toString())  }
            .fold(true, {acc, next -> acc && next })
}

//(requiredKey.first) && requiredKey.second()

fun isValidBYR(year: String): Boolean {
    return try {
        year.toInt() in 1920..2002
    } catch (e: NumberFormatException) {
        false
    }

}
fun isValidIYR(year: String): Boolean {
    return try {
        year.toInt() in 2010..2020
    } catch (e: NumberFormatException) {
        false
    }
}

fun isValidEYR(year: String): Boolean {
    return try {
        year.toInt() in 2020..2030
    } catch (e: NumberFormatException) {
        false
    }
}

fun isValidHGT(height: String): Boolean {
    return if (height.endsWith("cm")) {
        height.length == 5 && height.substring(0, 3).toInt() in 150..193
    } else if (height.endsWith("in")) {
        height.length == 4 && height.substring(0, 2).toInt() in 59..76
    } else false
}

fun isValidHCL(color: String): Boolean {
    return color[0] == '#' && color.substring(1).toCharArray().map { c -> c.isDigit() || setOf('a', 'b', 'c', 'd', 'e', 'f', 'g').contains(c) }
            .fold(true, {acc, next -> acc && next})
}

fun isValidECL(color: String): Boolean {
    return setOf("amb", "blu", "brn", "gry", "grn", "hzl", "oth").contains(color)
}

fun isValidPID(pid: String): Boolean {
    return pid.length == 9 && pid.toCharArray().map { c -> c.isDigit() }.fold(true, {acc, next -> acc && next})
}


fun testThings() {
    println(isValidBYR("2002"))
    println(!isValidBYR("2003"))

    println(isValidHGT("60in"))
    println(isValidHGT("190cm"))
    println(!isValidHGT("190in"))
    println(!isValidHGT("190"))

    println(isValidHCL("#123abc"))
    println(!isValidHCL("#123abz"))
    println(!isValidHCL("123abc"))

    println(isValidECL("brn"))
    println(!isValidECL("wat"))

    println(isValidPID("000000001"))
    println(!isValidPID("0123456789"))
}