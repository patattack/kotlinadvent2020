package code

import java.io.File
import java.util.*


fun main() {
    val lines = File("/Users/pbrady/dev/kotlinAdvent2020/src/inputs/day14.txt")
            .useLines { it.toList() }

    // Part 1
    var mask = getMasks(lines.first())
    val memory = mutableMapOf<Int, Long>()
    lines.map { line ->
        if (line.startsWith("mask")) mask = getMasks(line)
        else setMemory(memory, mask, line)
    }
    println(memory.values.sum())

    // Part 2
    var maskV2 = getMasksV2(lines.first())
    val memoryV2 = mutableMapOf<Long, Long>()
    lines.map { line ->
        if (line.startsWith("mask")) maskV2 = getMasksV2(line)
        else setMemoryV2(memoryV2, maskV2, line)
    }
    println(memoryV2.values.sum())

}


fun setMemoryV2(memory: MutableMap<Long, Long>, masks: Pair<List<Int>, BitSet>, line: String) {
    val value = line.substringAfter('=').trim().toLong()
    val memLocation = convert(line.substringAfter('[').substringBefore(']').toLong())
    memLocation.or(masks.second) // Set the 1 bits to 1.

    val allLocations = masks.first.fold(listOf(memLocation), { clearHalf, floatingIndex->
        clearHalf.map { it.clear(floatingIndex) }
        val setHalf= clearHalf.map { (it.clone() as BitSet) }
        setHalf.map { it.set(floatingIndex) }
        clearHalf + setHalf
    })

    allLocations.map { memory[convert(it)] = value }
//    memory[memLocation] = value
}

fun setMemory(memory: MutableMap<Int, Long>, masks: Pair<BitSet, BitSet>, line: String) {
    val memLocation = line.substringAfter('[').substringBefore(']').toInt()
    val valueSet = convert(line.substringAfter('=').trim().toLong())
    valueSet.and(masks.first)
    valueSet.or(masks.second)
    memory[memLocation] = convert(valueSet)
}

// https://stackoverflow.com/questions/2473597/bitset-to-and-from-integer-long
fun convert(value: Long): BitSet {
    var value = value
    val bits = BitSet(36)
    var index = 0
    while (value != 0L) {
        if (value % 2L != 0L) {
            bits.set(index)
        }
        ++index
        value = value ushr 1
    }
    return bits
}

// https://stackoverflow.com/questions/2473597/bitset-to-and-from-integer-long
fun convert(bits: BitSet): Long {
    var value = 0L
    for (i in 0 until bits.length()) {
        value += if (bits[i]) 1L shl i else 0L
    }
    return value
}

fun getMasksV2(line: String): Pair<List<Int>, BitSet> {
    val (floatingMask, orMask) = line.substringAfter('=').trim().toCharArray().reversed().foldIndexed(Pair(listOf<Int>(), BitSet(36)), {
        index, masks, c ->
        when (c) {
            'X' -> (masks.first + index to masks.second)
            '1' -> {
                masks.second.set(index);
                masks
            }
            else -> masks
        }
    })

    return Pair(floatingMask, orMask)
}

fun getMasks(line: String): Pair<BitSet, BitSet> {
    val (andMask, orMask) = line.substringAfter('=').trim().toCharArray().reversed().foldIndexed(Pair(BitSet(36), BitSet(36)), {
        index, masks, c ->
        when (c) {
            '0' -> masks.first.set(index)
            '1' -> masks.second.set(index)
        }
        masks
    })
    andMask.flip(0, 36) // set them all to 1 except the bits in question
    return Pair(andMask, orMask)
}
