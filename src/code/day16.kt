package code

import java.io.File

class ValidNumbers(private val ranges: List<IntRange>) {
    constructor() : this(emptyList())

    fun addRange(min: Int, max: Int) : ValidNumbers {
        return ValidNumbers(ranges.plusElement(min.rangeTo(max)))
    }

    fun contains(query: Int) : Boolean {
        for (r in ranges) {
            if (r.contains(query)) return true
        }
        return false
    }
}

// Im trying out a mutable version of this
class TicketDeducer() {
    private val rules = mutableMapOf<String, Pair<IntRange, IntRange>>() // field name -> possible ranges
    private lateinit var data: List<List<Int>> // ticket index -> list of entries for that location
    private val possibilities =  mutableMapOf<Int, MutableSet<String>>() // ticket index -> all field name options for that index
    private val solved = mutableMapOf<String, Int>()

    fun addRules(name: String, r1: IntRange, r2: IntRange) {
        rules[name] = r1 to r2
    }

    fun addValidTickets(tickets: List<String>) {
        data = tickets.map { line -> line.split(',').map { it.toInt() } }
    }

    fun computeMeanings() : Boolean{
        for (i in 0..data.first().lastIndex) {
            possibilities[i] = mutableSetOf(*rules.keys.toTypedArray())
        }

        // put in initial things to narrow down possibilities
        data.map { ticket ->
            ticket.mapIndexed { idx, num ->
                val validFields = rules.filter { it.value.first.contains(num) || it.value.second.contains(num) }.keys
                possibilities[idx] = validFields.intersect(possibilities.getOrDefault(idx, emptySet())).toMutableSet()
            }
        }

        //find the one(s) with one possibility and remove them as other options
        repeat(data.first().size) {
            val newlySolved = possibilities.filter { it.value.size == 1 }
            solved += newlySolved.map { it.value.first() to it.key }.toMutableList() // Store for later

            val solvedIndexes = newlySolved.values.map { it.first() }
            possibilities.replaceAll { _, u ->  (u - solvedIndexes).toMutableSet()}
        }

            // If all possibilities have been narrowed down return
        if (possibilities.filter { it.value.size != 0 }.count() == 0) return true

        return false
    }

    fun getIndexes(vararg indexes: String) : List<Int> {
        return indexes.map { solved.getOrDefault(it, -1) }
    }
}

fun main() {
    val (rules, ownTicket, nearby) = File("/Users/pbrady/dev/kotlinAdvent2020/src/inputs/day16.txt")
            .readText().split("\n\n")
    val ticketDeducer = TicketDeducer()
    val allRanges = rules.split('\n').fold(ValidNumbers(), {
        vn, rule ->
        val validRanges = parseRule(rule)
        ticketDeducer.addRules(rule.substringBefore(':'),
                validRanges[0].first.rangeTo(validRanges[0].second),
                validRanges[1].first.rangeTo(validRanges[1].second)) // ugly lol
        validRanges.fold(vn, {acc, p -> acc.addRange(p.first, p.second) }) // I kinda like this version better
    })

    val nearbyTickets = nearby.split('\n')

    // Part 1
    val allErrors = nearbyTickets.subList(1, nearbyTickets.size).fold(emptyList<Int>(), {
        errors, line ->
        errors + line.split(',').map { it.toInt() }.filter { !allRanges.contains(it) }
    })
    println(allErrors.sum())

    // Part 2

    val validTickets = nearbyTickets.subList(1, nearbyTickets.size).filter { line ->
        line.split(',').map { it.toInt() }.fold(true, { found, num -> found && allRanges.contains(num) })
    } + ownTicket.split("\n")[1]

    ticketDeducer.addValidTickets(validTickets)
    println("Found solution: ${ticketDeducer.computeMeanings()}")
    val indexes = ticketDeducer.getIndexes("departure location", "departure station", "departure platform",
            "departure track", "departure date", "departure time")
    val parsedTicket = ownTicket.split("\n")[1].split(',').map { it.toLong() }
    println(indexes.map { parsedTicket[it]}.reduce { l, r -> l * r })
}

fun parseRule(rule: String) : List<Pair<Int, Int>> {
    val firstRange = rule.substringAfter(':').substringBefore("or").trim()
    val secondRange = rule.substringAfter("or ")
    return listOf(parseRange(firstRange), parseRange(secondRange))
}

fun parseRange(r: String) = r.substringBefore('-').toInt() to r.substringAfter('-').toInt()
