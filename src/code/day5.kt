package code

import java.io.File

class Seat(location: String) {
    private val location = parseLocation(location)

    private fun parseLocation(location: String): Pair<Int, Int> {
        var value = 0
        var factor = 64
        for (c in location.substring(0, 7)) {
            if (c == 'B') value += factor
            factor /= 2
        }
        val row = value

        value = 0
        factor = 4
        for (c in location.substring(7, 10)) {
            if (c == 'R') value += factor
            factor /= 2
        }
        return Pair(row, value)
    }

    fun getID(): Int = location.first * 8 + location.second

}

fun main() {
    val lines = File("/Users/pbrady/dev/kotlinAdvent2020/src/inputs/day5.txt")
            .useLines { it.toList() }
    println(lines.map{line -> Seat(line).getID()}.max())
    var prevId = 0
    for (id in lines.map{line -> Seat(line).getID()}.sorted()) {
        if (id - prevId > 1 && prevId != 0) {
            println(id - 1)
        }
        prevId = id
    }
}