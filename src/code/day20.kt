package code

import java.io.File

class BuiltPuzzle(val map: Map<Pair<Int, Int>, Tile> = emptyMap()) {
    fun score(puzzleDimenstion: Int) : Long {
        return map[0 to 0]!!.id.toLong() *
                map[0 to puzzleDimenstion-1]!!.id.toLong() *
                map[puzzleDimenstion-1 to 0]!!.id.toLong() *
                map[puzzleDimenstion-1 to puzzleDimenstion-1]!!.id.toLong()
    }
    fun getFarthestLocation() = map.keys.sortedWith(LocationCompare()).first()
    fun placedTiles(): Set<Int> = setOf(*map.values.map { it.id }.toTypedArray())
//    fun getRequiredMatches() : List<String> {
//        getFarthestLocation()
//    }  //left OR left and top OR top
}

class LocationCompare: Comparator<Pair<Int, Int>>{
    override fun compare(p1: Pair<Int, Int>, p2: Pair<Int, Int>): Int {
        val p1val = p1.first * 1000 + p1.second // 1000 is hardcoded but could e the max of second elem
        val p2val = p2.first * 1000 + p2.second
        return p2val - p1val
    }
}

class Tile(tileInfo: Pair<Int, List<String>>) { // left, top, right, bottom
    val id = tileInfo.first
    private val borders = tileInfo.second

    fun left() = borders[0]
    fun top() = borders[1]
    fun right() = borders[2]
    fun bottom() = borders[3]

    object TileFactory{
        val permutationTracker = mutableMapOf<String, List<Tile>>() // track left hand side ONLY
        fun create(tileInfo: String): Tile {
            val tId = tileInfo.substringAfter("Tile ").substringBefore(':').toInt()
            val tContents = tileInfo.split('\n').drop(1)
            val tBorders = listOf(
                    tContents.map { it.first() }.joinToString("").reversed(),
                    tContents.first(),
                    tContents.map { it.last() }.joinToString(""),
                    tContents.last().reversed()
            )

            val tileVariations = Tile(tId to tBorders).allVariations()
            tileVariations.map { addPerm(it.left(), it) }

            return Tile(tId to tBorders)
        }

        fun addPerm(s: String, t: Tile) {
            permutationTracker[s] = permutationTracker.getOrDefault(s, emptyList()) + t
        }
    }

    fun allVariations(): List<Tile> {
        return listOf(
                this,
                this.rotateTile(),
                this.rotateTile().rotateTile(),
                this.rotateTile().rotateTile().rotateTile(),

                this.flipVerticalAxis(),
                this.flipVerticalAxis().rotateTile(),
                this.flipVerticalAxis().rotateTile().rotateTile(),
                this.flipVerticalAxis().rotateTile().rotateTile().rotateTile()
        )
    }


    fun rotateTile() = Tile(id to borders.subList(1, 4) + borders[0])

    fun rotateTileRight() = Tile(id to listOf(borders[3]) + borders.subList(0, 3))

    private fun flipVerticalAxis() = Tile(id to listOf(borders[2].reversed(), borders[1].reversed(),borders[0].reversed(), borders[3].reversed()))

//    fun flipHorizontalAxis() = Tile(id to listOf(borders[0].reversed(), borders[3].reversed(), borders[2].reversed(), borders[1].reversed()))

}

fun main() {
    val tileInfo = File("/Users/pbrady/dev/kotlinAdvent2020/src/inputs/day20.txt").readText().split("\n\n")
    val tiles = tileInfo.map { Tile.TileFactory.create(it) }
    val puzzleDim = Math.sqrt(tiles.count().toDouble()).toInt()

    for (t in tiles) {
        t.allVariations().forEach { tileVariation ->
            val initialMap = mapOf((0 to 0) to tileVariation)
            when (val p = buildPuzzle(BuiltPuzzle(initialMap), puzzleDim)) {
                null -> println("Unsuccessful")
                else -> {
                    println("Success!")
                    println(p.score(puzzleDim))
                    return
                }
            }
        }
    }

}

fun buildPuzzle(existingPuzzle: BuiltPuzzle, finalPuzzleDim: Int) : BuiltPuzzle? {
    if (existingPuzzle.map.count() == finalPuzzleDim * finalPuzzleDim) return existingPuzzle
    val placedTiles = existingPuzzle.placedTiles()
    val farthestExisting = existingPuzzle.getFarthestLocation()
    val nextLocationInt = farthestExisting.first * finalPuzzleDim + farthestExisting.second + 1
    val nextLocation = nextLocationInt.div(finalPuzzleDim) to nextLocationInt.rem(finalPuzzleDim)
    if (existingPuzzle.map.count() == 8){
        print('a')
    }
    if (nextLocation.first == 0) {
        // Only do the left part
        val mustMatch = existingPuzzle.map[farthestExisting]!!.right().reversed()
        val nextOptions = Tile.TileFactory.permutationTracker[mustMatch]!!.filter { !placedTiles.contains(it.id) }
        for (option in nextOptions) {
            val attempt = buildPuzzle(BuiltPuzzle(existingPuzzle.map + (nextLocation to option)), finalPuzzleDim)
            if (attempt != null)  return attempt
        }
    } else if (nextLocation.second == 0) {
        // match only the top
        val mustMatch = existingPuzzle.map[nextLocation.first-1 to 0]!!.bottom().reversed()
        val nextOptions = Tile.TileFactory.permutationTracker[mustMatch]!!.filter { !placedTiles.contains(it.id) }
        for (option in nextOptions) {
            val attempt = buildPuzzle(BuiltPuzzle(existingPuzzle.map + (nextLocation to option.rotateTileRight())), finalPuzzleDim)
            // rotate the tile so that the side that matched
            if (attempt != null)  return attempt // TONSDKFJHsaf
        }
    } else {
        // general case - match the left and top
        val mustMatchLeft = existingPuzzle.map[farthestExisting]!!.right().reversed()
        val mustMatchTop = existingPuzzle.map[nextLocation.first-1 to nextLocation.second]!!.bottom().reversed()
        val nextOptions = Tile.TileFactory.permutationTracker[mustMatchLeft]!!.filter { !placedTiles.contains(it.id) }
        for (option in nextOptions) {
            if (option.top() != mustMatchTop) continue
            val attempt = buildPuzzle(BuiltPuzzle(existingPuzzle.map + (nextLocation to option)), finalPuzzleDim)
            if (attempt != null)  return attempt
        }
    }
    return null
}