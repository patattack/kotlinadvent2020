package code

import java.io.File

class AllBags {
    private val bags = mutableMapOf<String, Bag>()

    fun identifyBag(bagName: String): Bag {
        val (color, _) = bagName.split(" bag")
        if (color in bags) return bags[color]!!
        val newBag = Bag(color)
        bags[color] = newBag
        return newBag
    }

    fun parseBagContents(bagContents: String): Array<Pair<Int, Bag>> {
        if (bagContents == "no other bags.") return arrayOf()
        return bagContents.split(", ").map { bagInfo -> Pair(bagInfo.substring(0,1).toInt(), identifyBag(bagInfo.substring(2))) }.toTypedArray()
    }


    fun countContainingGolden(allBags: MutableMap<String, Bag> = this.bags): Int {
        val goldenTracker = mutableMapOf<String, Boolean>()
        return allBags.map { pair -> pair.value.containsGolden(goldenTracker) }.count { hasGolden -> hasGolden }
    }

    fun countWithinGolden(allBags: MutableMap<String, Bag> = this.bags): Int? {
        val countTracker = mutableMapOf<String, Int>()
        return allBags["shiny gold"]!!.numBagsInside(countTracker)
    }

    override fun toString(): String = bags.values.toString()
}

class Bag(private var color: String, vararg bags: Pair<Int, Bag>) {
    private var children = bags

    fun setChildren(vararg bags: Pair<Int, Bag>) {
        children = bags
    }

    fun containsGolden(goldenTracker: MutableMap<String, Boolean>): Boolean {
        if (children.isEmpty()) {
            goldenTracker[color] = false
            return false
        }
        return children.map { child ->
            goldenTracker.getOrPut(child.second.color, { child.second.color == "shiny gold" || child.second.containsGolden(goldenTracker) }) }
                .fold(false, {acc, found -> acc || found })

    }

    fun numBagsInside(countTracker: MutableMap<String, Int>): Int {
        if (children.isEmpty()) {
            countTracker[color] = 0
            return 0
        }
        return children.map { child ->
            child.first + (child.first * countTracker.getOrPut(child.second.color, { child.second.numBagsInside(countTracker) }))
        }.fold(0, { total, child -> total + child})
    }

    override fun toString(): String = "$color: ${children.map { pair -> "${pair.first} ${pair.second.color}" } }"
}

fun main() {
    val lines = File("/Users/pbrady/dev/kotlinAdvent2020/src/inputs/day7.txt")
            .useLines { it.toList() }
    val allBags = AllBags()
    lines.map { line ->
        val rule = line.split(" contain ")
        val containingBag = allBags.identifyBag(rule[0])
        containingBag.setChildren(*allBags.parseBagContents(rule[1]))
      }
    println(allBags.countContainingGolden())
    println(allBags.countWithinGolden())
    println("done")
}



