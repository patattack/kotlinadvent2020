package code

import java.io.File

fun main() {
    val originalLines = File("/Users/pbrady/dev/kotlinAdvent2020/src/inputs/day11.txt")
            .useLines { it.toList() }

    var previous_lines = originalLines
    var newLines = seatPeopleSight(originalLines)
    println(newLines)
    while (previous_lines.hashCode() != newLines.hashCode()) {
        previous_lines = newLines
        newLines = seatPeopleSight(previous_lines)
    }

    println(newLines)
    println(newLines.map { s -> s.toCharArray().count { c -> c =='#' } }.sum())
}

// Part 1 -----------------------
fun seatPeopleAdj(lines: List<String>): List<String> {
    return lines.mapIndexed { colIndex, row -> row.toCharArray().mapIndexed() { rowIndex, c ->
        if (c == 'L' && countImmediateAdjOccupied(lines, rowIndex, colIndex) == 0) { '#' }
        else if (c == '#' && countImmediateAdjOccupied(lines, rowIndex, colIndex) >= 4) {'L'}
        else c
    }.joinToString(separator = "") { c -> c.toString() } }
}

fun countImmediateAdjOccupied(lines: List<String>, row: Int, col: Int): Int {
    return listOf(Pair(row + 1, col + 1), Pair(row + 1, col ), Pair(row + 1, col -1),
            Pair( row, col + 1), Pair(row, col -1),
            Pair( row - 1, col + 1), Pair(row - 1, col ), Pair(row - 1, col -1))
            .filter { p -> p.first >= 0 && p.first < lines.first().length && p.second >= 0 && p.second < lines.size}
            .filter { p-> lines[p.second][p.first] == '#'}.count()
}

//Part 2 -------------------------

fun seatPeopleSight(lines: List<String>): List<String> {
    return lines.mapIndexed { colIndex, row -> row.toCharArray().mapIndexed() { rowIndex, c ->
        if (c == 'L' && countSightAdjOccupied(lines, rowIndex, colIndex) == 0) { '#' }
        else if (c == '#' && countSightAdjOccupied(lines, rowIndex, colIndex) >= 5) {'L'}
        else c
    }.joinToString(separator = "") { c -> c.toString() } }
}

fun countSightAdjOccupied(lines: List<String>, row: Int, col: Int): Int {
    val test =  listOf(Pair(1,  1), Pair( 1, 0 ), Pair(1, -1),
            Pair( 0, 1), Pair(0, -1),
            Pair( -1,  1), Pair(-1, 0), Pair( -1, -1))
            .filter { pair -> findInDir(lines, row, col, pair.first, pair.second) }.count()
    return test
}

fun findInDir(lines: List<String>, row: Int, col: Int, rowDelta: Int, colDelta: Int): Boolean {
    var rowIndex = row + rowDelta
    var colIndex = col + colDelta
    while( rowIndex >= 0 && rowIndex < lines.first().length && colIndex >= 0 && colIndex < lines.size ) {
        val c = lines[colIndex][rowIndex]
        if (c == '#') return true
        if (c == 'L') return false
        colIndex += colDelta
        rowIndex += rowDelta
    }
    return false
}