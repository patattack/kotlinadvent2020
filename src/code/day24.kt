package code

import java.io.File

fun main() {
    val directions =  File("/Users/pbrady/dev/kotlinAdvent2020/src/inputs/day24.txt").readLines().map { parseLine(it) }
    val locations = directions.map { directionsToLocation(it) }
    val countOfLocations = locations.fold(emptyMap<Pair<Double, Int>, Int>(), {
        m, loc ->
        m.plus(loc to m.getOrDefault(loc, 0) + 1)
    })
    val blackTiles = countOfLocations.filter { it.value.rem(2) == 1 }.count()
    println(blackTiles)
}

/*
    Converts a line into a list of the separate instructions
 */
fun parseLine(line: String) : List<String> {
    val (parsedDirections, _) = line.fold(emptyList<String>() to "", {
        (directions, partialDirection), c ->
        when (c){
            'e' -> directions + partialDirection.plus("e") to ""
            'w' -> directions + partialDirection.plus("w") to ""
            'n' -> directions to "n"
            else -> directions to "s"
        }
    })
    return parsedDirections
}

fun directionsToLocation(dirs: List<String>) : Pair<Double, Int> {
    // im using .5 in the x direction to be half away from one hex and 1 being the one a full one away. height is always in exact rows.
    return dirs.fold(0.0 to 0, { loc, dir ->
        when(dir) {
            "e" -> loc.first + 1 to loc.second
            "w" -> loc.first - 1 to loc.second
            "ne" -> loc.first + 0.5 to loc.second + 1
            "nw" -> loc.first - 0.5 to loc.second + 1
            "se" -> loc.first + 0.5 to loc.second - 1
            else -> loc.first - 0.5 to loc.second - 1
        }
    })
}