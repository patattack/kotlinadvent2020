package code

import java.io.File

class Rules(private val rules: Map<Int, Rule>) {
    constructor():  this(emptyMap<Int, Rule>())

    fun addRule(line: String) : Rules {
        val id = line.substringBefore(':').toInt()
        val composition = line.substringAfter(':').trim()
        return Rules(rules + (id to Rule(id, composition)))
    }

    fun match(input: String) : Boolean {
        val regExCatalog = emptyMap<Int, String>()
        val rootRule = rules[0] ?: return false
        val allRegex = rootRule.generateRegEx(this, regExCatalog)
        return allRegex[0]?.toRegex()?.matches(input) ?: false
    }

    fun getRule(id: Int) = rules[id]
}

class Rule(private val id: Int, private val r: String) {
//    lateinit var regexForm: String

    fun generateRegEx(rules: Rules, inputCatalog: Map<Int, String>) : Map<Int, String> {
        if (id in inputCatalog) return inputCatalog // Already computed. no more changes needed.

        // Recursively get all the necessary rules into the catalog
        val updatedRegExCatalog = r.split(" ").fold(inputCatalog, { regCat, part ->
            when (part.toIntOrNull()) {
                null -> regCat
                else -> rules.getRule(part.toInt())!!.generateRegEx(rules, regCat)
            }
        })

        // Base case
        if (r.contains('"')) return updatedRegExCatalog + (id to r.substringAfter('"').substringBefore('"'))

        // Composed Case
        val finalRegex = r.split(' ').fold("(", {reg, next ->
            when {
                next[0] == '|' -> "$reg)|("
                else -> reg + "(${updatedRegExCatalog[next.toInt()]})"
            }
        }) + ')'


        return updatedRegExCatalog + (id to finalRegex)

    }

//    private fun mergeRules(regEx: Map<Int, String>, nums: List<Int>) {
//
//    }
}

fun main() {
    val (ruleLines, testCases) = File("/Users/pbrady/dev/kotlinAdvent2020/src/inputs/day19.txt")
            .readLines().fold(listOf<String>() to listOf<String>(), {lists, s ->
                if (s.contains(':')) lists.first + s to lists.second
                else lists.first to lists.second + s
            })

    val allRules = ruleLines.fold(Rules(), {rules, line -> rules.addRule(line)})
    val ruleMatches = testCases.subList(1, testCases.size).map { allRules.match(it) }
    println(ruleMatches)
    println(ruleMatches.filter { it }.count())

}