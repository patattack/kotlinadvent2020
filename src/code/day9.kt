package code

import java.io.File

fun main() {
    val lines = File("/Users/pbrady/dev/kotlinAdvent2020/src/inputs/day9.txt")
            .useLines { it.toList() }.map { number -> number.toLong() }
    val invalidNumber = findFirstError(25, lines)
    println(invalidNumber)
    val subarray = slidingWindow(invalidNumber, lines)
    println(subarray)
    println(subarray.min()?.plus(subarray.max()!!))
}

fun slidingWindow(targetSum: Long, lines: List<Long>): List<Long> {
    var startIdx = 0
    var endIdx = 1
    while(endIdx < lines.size) {
        val total = lines.subList(startIdx, endIdx).sum()
        when {
            total == targetSum -> return lines.subList(startIdx, endIdx)
            total > targetSum -> startIdx++
            total < targetSum -> endIdx++
        }
    }
    return listOf(-1, -1)
}

fun findFirstError(preamble: Int, lines: List<Long>): Long {
    return lines.subList(preamble, lines.size).filterIndexed {
        index, current ->
        !hasPairWithSum(current, lines.subList(index, index + preamble) )
    }.first()
}

fun hasPairWithSum(sum: Long, lines: List<Long>): Boolean {
    return (lines.subList(0, lines.size - 1).filterIndexed {
        idx, value->
        lines.subList(idx+1, lines.size).contains(sum - value)
    }.count() >= 1)
}

