package code

import java.io.File
import kotlin.math.PI
import kotlin.math.absoluteValue
import kotlin.math.cos
import kotlin.math.sin

class Waypoint(private val location: List<Int>) {
    // location in terms of [N, E, S, W]

    fun moveWaypoint(deltaX: Int, deltaY: Int): Waypoint {
        var newLocation = mutableListOf<Int>(0, 0, 0, 0)
        val y = location[0] + deltaY - location[2]
        if (y > 0) newLocation[0] = y
        if (y < 0) newLocation[2] = -y

        val x = location[1] + deltaX - location[3]
        if (x > 0) newLocation[1] = x
        if (x < 0) newLocation[3] = -x

        return Waypoint(newLocation)
    }

    fun rotate(degrees: Int): Waypoint {
        val indexShift= degrees / 90
        return Waypoint(location.mapIndexed { index, _ -> location[Math.floorMod(index + indexShift, 4)]})
    }

    fun y(): Int = location[0] - location[2]
    fun x(): Int = location[1] - location[3]
}

fun main() {
    val lines = File("/Users/pbrady/dev/kotlinAdvent2020/src/inputs/day12.txt")
            .useLines { it.toList() }

    // PART 1
    val finalState = lines.fold(Pair(Pair(0, 0), 0), { previousInfo, action ->
        val parsedAction = parseAction(action, previousInfo.second)
        Pair(Pair(previousInfo.first.first + parsedAction.first.first,
                previousInfo.first.second + parsedAction.first.second),
                previousInfo.second + parsedAction.second) })
    println(finalState)
    println(finalState.first.first.absoluteValue + finalState.first.second.absoluteValue)

    // waypoint of format
    // PART 2
    val finalState2 = lines.fold(Pair(Pair(0, 0), Waypoint(listOf(1,10, 0, 0))), { previousInfo, action ->
        val a = parseWaypointAction(action, previousInfo.first, previousInfo.second)
        a
    })
    println(finalState2)
    println(finalState2.first.first.absoluteValue + finalState2.first.second.absoluteValue)
}

fun parseWaypointAction(action: String, shipLocation: Pair<Int, Int>, waypoint: Waypoint): Pair<Pair<Int, Int>, Waypoint> {
    val letter = action[0]
    val amount = action.substring(1).toInt()
    return when (letter) {
        'N' -> Pair(shipLocation, waypoint.moveWaypoint(0,  amount))
        'S' -> Pair(shipLocation, waypoint.moveWaypoint(0, -amount))
        'E' -> Pair(shipLocation, waypoint.moveWaypoint(amount, 0))
        'W' -> Pair(shipLocation, waypoint.moveWaypoint(-amount, 0))
        'L' -> Pair(shipLocation, waypoint.rotate(amount))
        'R' -> Pair(shipLocation, waypoint.rotate(-amount))
        else -> Pair(Pair(
                shipLocation.first + amount * waypoint.x(),
                shipLocation.second + amount * waypoint.y()), waypoint) // Forward
    }
}

fun parseAction(action: String, currentDirectionDegrees: Int): Pair<Pair<Int, Int>, Int> {
    val letter = action[0]
    val amount = action.substring(1).toInt()
    return when (letter) {
        'N' -> Pair(Pair(0, amount), 0)
        'S' -> Pair(Pair(0, -amount), 0)
        'E' -> Pair(Pair(amount, 0), 0)
        'W' -> Pair(Pair(-amount, 0), 0)
        'L' -> Pair(Pair(0, 0), amount)
        'R' -> Pair(Pair(0, 0), -amount)
        else -> Pair(Pair(
                amount * cos(currentDirectionDegrees.toDouble() * (2 * PI / 360)).toInt(),
                amount * sin(currentDirectionDegrees.toDouble() * (2 * PI / 360)).toInt()
        ), 0) // Forward
    }
}