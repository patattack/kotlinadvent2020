package code

import java.io.File

interface  Memory {
    fun getIthNumber(i: Int): Int
    fun generateNext(): Memory
    fun compute(iterations: Int): Int
}


class BasicMemory (private val store: List<Int>)  {
//    constructor(store: List<Int>) : { val store = store }
    constructor(): this(listOf<Int>())

    private fun generateNext(): BasicMemory {
        val lastIndex = store.dropLast(1).indexOfLast { it == store.last() }
        val nextNumber = when {
            lastIndex >= 0 -> store.size - (lastIndex + 1) // Adding one because the indexes are off
            else -> 0
        }
        return BasicMemory(store.plus(nextNumber))
    }

    fun getIthNumber(i: Int): Int = store[i-1]

    fun compute(iterations: Int): Int {
        var memory = this
        repeat(iterations) {
            memory = memory.generateNext()
        }
        return memory.getIthNumber(iterations)
    }
}

class AdvancedMemory(initialList: List<Int>) {
    private val store: MutableMap<Int, Pair<Int, Int>> = mutableMapOf(*initialList.mapIndexed { index, i -> i to Pair(-1, index+1) }.toTypedArray())
    var prevNumber = initialList.last()
    var newIndex = store.size + 1

    fun generateNext() {
        val (lastIndex, _) = store.getOrDefault(prevNumber, -1 to -1)
        val nextNumber = when {
            lastIndex >= 0 -> newIndex - 1 - lastIndex
            else -> 0
        }
        store[nextNumber] = (store.getOrDefault(nextNumber, -1 to -1).second to newIndex)
        prevNumber = nextNumber
        newIndex++
    }

    fun getIthNumber(i: Int): Int {
        return store.filter { it.value.second == i }.keys.first()
    }

    fun compute(iterations: Int): Int {
        repeat(iterations) {
            generateNext()
        }
        return getIthNumber(iterations)
    }

}

fun main() {
    val numbers = File("/Users/pbrady/dev/kotlinAdvent2020/src/inputs/day15.txt").readLines().first()
            .split(',').map { it.toInt() }

    println(BasicMemory(numbers).compute(2020))
    println(AdvancedMemory(numbers).compute(30000000))
}
