package code

import java.io.File

fun main() {
    var lines = File("/Users/pbrady/dev/kotlinAdvent2020/src/inputs/day8.txt")
            .useLines { it.toList() }.toMutableList()
//    val (_, acc) = runSimulation(lines)
//    print(acc)
    for (i in 0..lines.size) {
        if (lines[i].substring(0, 3) == "nop") {
            lines[i] = lines[i].replaceRange(0, 3, "jmp")
            val (success, acc) = runSimulation(lines)
            if (success) {
                println(acc)
                break
            }
            lines[i] = lines[i].replaceRange(0, 3, "nop")
        }
        if (lines[i].substring(0, 3) == "jmp") {
            lines[i] = lines[i].replaceRange(0, 3, "nop")
            val (success, acc) = runSimulation(lines)
            if (success) {
                println(acc);
                break
            }
            lines[i] = lines[i].replaceRange(0, 3, "jmp")
        }
    }
}

fun runSimulation(lines: List<String>): Pair<Boolean, Int> {
    var visited = mutableSetOf<Int>()
    var index = 0
    var accumulator = 0
    while (true) {
        val (acc, offset) = parseInstruction(lines[index])
        if (index in visited) return Pair(false, acc)
        visited.add(index)
        index += offset
        accumulator += acc
        if (index == lines.size) return Pair(true, accumulator)
    }
}

fun parseInstruction(line: String): Pair<Int, Int> {
    return when(line.substring(0, 3)) {
        "nop" -> Pair(0, 1)
        "acc" -> Pair(line.substring(4,line.length).toInt(), 1)
        else -> Pair(0, line.substring(4,line.length).toInt())
    }
}