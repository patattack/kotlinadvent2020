package code

import java.io.File

fun findLoopIterations(cardKey: Long, doorKey: Long) : Pair<Long, Long> {
    var i = 0L
    val sn = 7L
    var value = 1L
    var cardLoops = -1L
    var doorLoops = -1L
    while (true) {
        value *= sn
        value = value.rem(20201227L)
        i += 1
        if (value == cardKey) {
            cardLoops = i
        }
        if (value == doorKey) {
            doorLoops = i
        }
        if (cardLoops != -1L && doorLoops != -1L) return cardLoops to doorLoops
    }
}

fun transform(sn: Long, loopSize: Long) : Long {
    var value = 1L
    repeat (loopSize.toInt()) {
        value *= sn
        value = value.rem(20201227L)
    }
    return value
}

fun main() {
    val (cardPKey, doorPKey) = File("/Users/pbrady/dev/kotlinAdvent2020/src/inputs/day25.txt").readLines().map { it.toLong() }
    val (cardLoop, doorLoop) = findLoopIterations(cardPKey, doorPKey)
    val encryptionKey = transform(doorPKey, cardLoop)
    println(encryptionKey)
}