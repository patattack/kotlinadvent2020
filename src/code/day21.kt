package code

import java.io.File

class FoodDeducer(private val currentDeductions: Map<String, Set<String>>) {
    constructor() : this(emptyMap())

    fun setUpRules(lines: List<String>) : FoodDeducer {
        val allOptionsPerAllergy = lines.fold(emptyMap<String, Set<String>>(), { allOptionsPerAllergy, line ->
            // All lines have one set of parenthesis to parse
            val ingredients = parseIngredients(line)
            val allergensToIngredients = parseAllergens(line).map {  it to allOptionsPerAllergy.getOrDefault(it, ingredients).intersect(ingredients) }
            allOptionsPerAllergy.plus(allergensToIngredients)
        })
        return FoodDeducer(allOptionsPerAllergy)
    }

    fun completeDeductions() : FoodDeducer {
        val knownAllergens = currentDeductions.filter { it.value.size == 1 }
        val newDeductions = currentDeductions.mapValues {
            if (knownAllergens.containsKey(it.key)) it.value
            else it.value - knownAllergens.values.flatten()
        }
        if (newDeductions == currentDeductions) return this // Base case - no new deductions are made
        return FoodDeducer(newDeductions).completeDeductions() // Compute new deductions recursively
    }

    fun allergenIngredients() = currentDeductions.flatMap { it.value }

    fun dangerousIngredientsList() : List<String> {
        // alphabetic by allergen
        return currentDeductions.entries.toList().sortedBy { entry -> entry.key }.map { it.value.first() }
    }

}

fun parseIngredients(line: String) = setOf(*line.substringBefore(" (contains").split(" ").toTypedArray())
fun parseAllergens(line: String) = line.substringAfter("contains ").substringBefore(')').split(", ")

fun main() {
    val lines = File("/Users/pbrady/dev/kotlinAdvent2020/src/inputs/day21.txt").readLines()
    val deducer = FoodDeducer().setUpRules(lines).completeDeductions()

    val allIngredientRecipes = lines.flatMap { parseIngredients(it) }
    val allergenIngredients = deducer.allergenIngredients()
    val allergenFreeIngredients = allIngredientRecipes.toSet() - allergenIngredients.toSet()
    val temp = allIngredientRecipes.filter { allergenFreeIngredients.contains(it) }
    println(temp.count())

    // Part 2
    deducer.dangerousIngredientsList().forEach { print("$it,") }

}