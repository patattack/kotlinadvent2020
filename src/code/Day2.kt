package code

import java.io.File

fun main() {
    val lines = File("/Users/pbrady/dev/kotlinAdvent2020/src/inputs/day2.txt")
        .useLines { it.toList() }
    var valid = 0
    var parts:List<String>
    var range:List<String>
    var first:Int
    var second:Int
    var letter:Char
    for (line in lines) {
        parts = line.split(' ')
        range = parts[0].split('-')
        first = range[0].toInt()
        second = range[1].toInt()
        letter = parts[1][0]

        if ((parts[2][first-1] == letter).xor( parts[2][second-1] == letter)) {
            valid++
            println(parts)
        }
    }
    println("valid: $valid")
}

fun part1() {
    val lines = File("/Users/pbrady/dev/kotlinAdvent2020/src/inputs/day2.txt")
        .useLines { it.toList() }
    var valid = 0
    var parts:List<String>
    var range:List<String>
    var lower:Int
    var upper:Int
    var letter:Char
    for (line in lines) {
        parts = line.split(' ')
        range = parts[0].split('-')
        lower = range[0].toInt()
        upper = range[1].toInt()
        letter = parts[1][0]

        val counts = parts[2].toCharArray().filter{it == letter}.count()
        if (lower <= counts && counts <= upper) {
            valid++
            println(parts)
        }
    }
    println("valid: $valid")
}