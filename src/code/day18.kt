package code

import java.io.File

enum class Precedence {
    SAME, DIFFERENT
}

class MathProblem(private var equation: String) {
    fun evaluate(precedence: Precedence): Long {
        while(equation.indexOf('(') != -1) {
            val startIndex = equation.indexOfFirst { it == '(' }
            val endIndex = startIndex + matchingParenIndex(equation.substring(startIndex))
            val result = MathProblem(equation.substring(startIndex +1, endIndex)).evaluate(precedence).toString()
            equation = equation.replaceRange(startIndex, endIndex + 1, result)
        }
        val operations = equation.split(' ')
        return when (precedence) {
            Precedence.SAME -> samePrecedenceCompute(operations)
            Precedence.DIFFERENT -> differentPrecedenceCompute(operations)
            else -> -1
        }


    }

    private fun differentPrecedenceCompute(operations: List<String>) : Long {
        // Fold with (nextMathString, )
        val additionComplete = operations.subList(1, operations.size).fold(listOf(operations.first()), { acc: List<String>, next: String ->
            when (acc.last()) {
                "+" -> acc.subList(0, acc.size - 2) + operate(acc[acc.size - 2].toLong(), "+", next.toLong()).toString() // Pop off the things and add them
                else -> acc + next // ignore and add
            }
        })
        return samePrecedenceCompute(additionComplete)
    }

    private fun samePrecedenceCompute(operations: List<String>) : Long {
        // Fold with (accumulator, lastOperand)
        return operations.subList(1, operations.size).fold(Pair(operations.first().toLong(), ""), { acc: Pair<Long, String>, next: String ->
            when (next) {
                "+" -> acc.first to "+"
                "*" -> acc.first to "*"
                else -> operate(acc.first, acc.second, next.toLong()) to "" // friends dont let friends write nested when statements
            }
        }).first
    }

    private fun operate(first: Long, operator: String, second: Long) : Long {
        return when (operator) {
            "+" -> first + second
            else -> first * second // Relies on good input operators
        }
    }

    private fun matchingParenIndex(search: String) : Int {
        search.foldIndexed(0, {idx, leftCount, c ->
            // Operate the next char
            val newCount = when (c) {
                '(' -> leftCount + 1
                ')' -> leftCount - 1
                else -> leftCount
            }
            if (newCount == 0) {
                return idx
            }
            newCount
            // Check to see if this was the matching parethesis
        })
        return -1
    }
}

fun main() {
    val mathProblems = File("/Users/pbrady/dev/kotlinAdvent2020/src/inputs/day18.txt")
            .useLines { it.toList() }.map { MathProblem(it) }
    var results = mathProblems.map { it.evaluate(Precedence.SAME) }
    println(results)
    println(results.sum())

    val part2results = mathProblems.map { it.evaluate(Precedence.DIFFERENT) }
    println(part2results)
    println(part2results.sum())
}